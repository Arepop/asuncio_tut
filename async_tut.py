import asyncio


async def handle_echo(reader, writer):
    data = await reader.read(100)
    message = data.decode()
    addr = writer.get_extra_info('peername')
    print("Received %r from %r" % (message, addr))

    print("Send: %r" % message)
    writer.write(data)
    await writer.drain()

    mess2 = message[message.find('/'): message.find('HTTP') bv-1]
    print(mess2)

    print("Close the client socket")
    writer.close()

    if mess2 == 'xxx':
        if len(asyncio.Task.all_tasks()) > 1:
            list(asyncio.Task.all_tasks())[-1].cancel()
        else:
            asyncio.ensure_future(print_number())
            print(asyncio.Task.all_tasks())


async def print_number():
    while True:
        print("working")
        await asyncio.sleep(1)

loop = asyncio.get_event_loop()
coro = asyncio.start_server(handle_echo, '127.0.0.1', 7800, loop=loop)
server = loop.run_until_complete(coro)

print('Serving on {}'.format(server.sockets[0].getsockname()))
try:
    loop.run_forever()
except KeyboardInterrupt:
    pass

server.close()
loop.run_until_complete(server.wait_closed())
loop.close()
